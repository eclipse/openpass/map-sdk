/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_LANEGROUP_H
#define MAPSDK_MAPAPI_LANEGROUP_H

#include "Common/common.h"
#include "lane.h"
#include "lane_boundary.h"

namespace map_api
{
struct LaneGroup
{
  enum class Type : std::uint8_t
  {
    kUnknown = 0,
    kOther,
    kOneWay,
    kJunction
  };

  Identifier id{UndefinedId};
  Type type{Type::kUnknown};

  std::vector<RefWrapper<LaneBoundary>> lane_boundaries;
  std::vector<RefWrapper<Lane>> lanes;
};

}  // namespace map_api

#endif  // ASTAS_THIRDPARTY_MAPSDK_INCLUDE_MAPAPI_LANEGROUP_H
