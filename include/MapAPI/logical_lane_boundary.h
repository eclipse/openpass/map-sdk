/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_LOGICLANEBOUNDARY_H
#define MAPSDK_MAPAPI_LOGICLANEBOUNDARY_H

#include <units.h>

#include <vector>

#include "Common/common.h"
#include "lane_boundary.h"
#include "reference_line.h"

namespace map_api
{
struct LogicalBoundaryPoint
{
  Position3d position;
  units::length::meter_t s_position;
  units::length::meter_t t_position;
};

struct LogicalLaneBoundary
{
  /// @ref osi3::LogicalLaneBoundary::PassingRule
  /// @see \link https://github.com/OpenSimulationInterface/open-simulation-interface/tree/v3.5.0 \endlink
  enum class PassingRule : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kNoneAllowed = 2,
    kIncreasingT = 3,
    kDecreasingT = 4,
    kBothAllowed = 5
  };

  Identifier id{UndefinedId};
  std::vector<LogicalBoundaryPoint> boundary_line;
  PassingRule passing_rule;
  ReferenceLine* reference_line;
  std::vector<RefWrapper<LaneBoundary>> physical_boundaries;
  std::vector<ExternalReference> source_references;
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_LOGICALLANEBOUNDARY_H
