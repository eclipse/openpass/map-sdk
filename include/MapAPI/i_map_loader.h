/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_IMAPLOADER_H
#define MAPSDK_MAPAPI_IMAPLOADER_H

#include <memory>
#include <string>

#include "map.h"

namespace map_api
{

class IMapLoader
{
public:
  virtual ~IMapLoader() = default;

  ///  @brief Load a map from the given file.
  ///  @param map_file The path to the map input file.
  ///  @return A unique pointer to the loaded map object. If the file cannot be opened or the contents  are invalid,
  /// an exception will be thrown.
  [[nodiscard]] virtual std::unique_ptr<Map> LoadMap(const std::string& map_file) const = 0;
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_IMAPLOADER_H
