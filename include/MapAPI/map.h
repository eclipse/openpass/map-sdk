/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_MAP_H
#define MAPSDK_MAPAPI_MAP_H

#include <limits>
#include <memory>
#include <string>
#include <vector>

#include "lane.h"
#include "lane_boundary.h"
#include "lane_group.h"
#include "logical_lane.h"
#include "logical_lane_boundary.h"
#include "reference_line.h"
#include "road_marking.h"
#include "stationary_object.h"
#include "traffic_light.h"
#include "traffic_sign.h"

namespace map_api
{

struct Map
{
  using Lanes = std::vector<std::unique_ptr<Lane>>;
  using LaneGroups = std::vector<std::unique_ptr<LaneGroup>>;
  using LaneBoundaries = std::vector<std::unique_ptr<LaneBoundary>>;
  using LogicalLanes = std::vector<std::unique_ptr<LogicalLane>>;
  using LogicalLaneBoundaries = std::vector<std::unique_ptr<LogicalLaneBoundary>>;
  using TrafficLights = std::vector<std::unique_ptr<TrafficLight>>;
  using TrafficSigns = std::vector<std::unique_ptr<TrafficSign>>;
  using ReferenceLines = std::vector<std::unique_ptr<ReferenceLine>>;
  using RoadMarkings = std::vector<std::unique_ptr<RoadMarking>>;
  using StationaryObjects = std::vector<std::unique_ptr<StationaryObject>>;

  std::string projection_string;
  std::string map_reference;
  std::uint32_t country_code{std::numeric_limits<std::uint32_t>::max()};

  Lanes lanes;
  LaneGroups lane_groups;
  LaneBoundaries lane_boundaries;
  LogicalLanes logical_lanes;
  LogicalLaneBoundaries logical_lane_boundaries;
  StationaryObjects stationary_objects;
  TrafficLights traffic_lights;
  TrafficSigns traffic_signs;
  ReferenceLines reference_lines;
  RoadMarkings road_markings;
};
}  // namespace map_api

#endif  // MAPSDK_MAPAPI_MAP_H
