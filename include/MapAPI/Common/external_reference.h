/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_COMMON_EXTERNALREFERENCE_H
#define MAPSDK_MAPAPI_COMMON_EXTERNALREFERENCE_H

#include <string>
#include <tuple>
#include <vector>

namespace map_api
{

struct ExternalReference
{
  std::string reference;
  std::string type;
  std::vector<std::string> identifiers;
};

inline bool operator==(const ExternalReference& lhs, const ExternalReference& rhs) noexcept
{
  return std::tie(lhs.reference, lhs.type, lhs.identifiers) ==
         std::tie(rhs.reference, rhs.type, rhs.identifiers);
}

inline bool operator!=(const ExternalReference& lhs, const ExternalReference& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_COMMON_EXTERNALREFERENCE_H
