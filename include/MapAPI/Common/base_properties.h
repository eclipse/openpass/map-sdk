/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_COMMON_BASEPROPERTIES_H
#define MAPSDK_MAPAPI_COMMON_BASEPROPERTIES_H

#include <vector>

#include "geometry.h"

namespace map_api
{
struct BaseProperties
{
  Dimension3d dimension;
  Position3d position;
  Orientation3d orientation;
  std::vector<Vector3d> base_polygon;
};

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_COMMON_BASEPROPERTIES_H
