/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_TRAFFICSIGN_H
#define MAPSDK_MAPAPI_TRAFFICSIGN_H

#include <vector>

#include "Common/common.h"
#include "traffic_sign_main_sign.h"
#include "traffic_sign_supplementary_sign.h"

namespace map_api
{

struct TrafficSign
{
  Identifier id{UndefinedId};
  MainSign main_sign;
  std::vector<SupplementarySign> supplementary_signs;
  std::vector<ExternalReference> source_references;
};
}  // namespace map_api

#endif  // MAPSDK_MAPAPI_TRAFFICSIGN_H
