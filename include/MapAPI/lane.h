/*******************************************************************************
 * Copyright (c) 2023-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (C) 2023, ANSYS, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MAPSDK_MAPAPI_LANE_H
#define MAPSDK_MAPAPI_LANE_H

#include <limits>
#include <vector>

#include "Common/common.h"
#include "lane_boundary.h"

namespace map_api
{
using LocalLaneId = std::int64_t;
constexpr LocalLaneId UndefinedLocalLaneId{std::numeric_limits<LocalLaneId>::max()};

struct Lane;

struct RoadCondition
{
  double surface_temperature;
  double surface_water_film;
  double surface_freezing_point;
  double surface_ice;
  double surface_roughness;
  double surface_texture;
};

inline constexpr bool operator==(const RoadCondition& lhs, const RoadCondition& rhs) noexcept
{
  return (mantle_api::AlmostEqual(lhs.surface_temperature, rhs.surface_temperature) &&
          mantle_api::AlmostEqual(lhs.surface_water_film, rhs.surface_water_film) &&
          mantle_api::AlmostEqual(lhs.surface_freezing_point, rhs.surface_freezing_point) &&
          mantle_api::AlmostEqual(lhs.surface_ice, rhs.surface_ice) &&
          mantle_api::AlmostEqual(lhs.surface_roughness, rhs.surface_roughness) &&
          mantle_api::AlmostEqual(lhs.surface_texture, rhs.surface_texture));
}

inline constexpr bool operator!=(const RoadCondition& lhs, const RoadCondition& rhs) noexcept
{
  return !(lhs == rhs);
}

struct Lane
{
  using PolylinePoint = Vector3d;
  using Polyline = std::vector<PolylinePoint>;

  enum class Type : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kDriving = 2,
    kNonDriving = 3,
    kIntersection = 4
  };

  enum class Subtype : std::uint8_t
  {
    kUnknown = 0,
    kOther = 1,
    kNormal = 2,
    kBiking = 3,
    kSidewalk = 4,
    kParking = 5,
    kStop = 6,
    kRestricted = 7,
    kBorder = 8,
    kShoulder = 9,
    kExit = 10,
    kEntry = 11,
    kOnRamp = 12,
    kOffRamp = 13,
    kConnectingRamp = 14
  };

  Identifier id{UndefinedId};
  LocalLaneId local_id{UndefinedLocalLaneId};
  Type type;
  Subtype sub_type;

  /// @note: order depends on centerline_is_driving_direction
  Polyline centerline;

  bool centerline_is_driving_direction;

  std::vector<RefWrapper<Lane>> left_adjacent_lanes;
  std::vector<RefWrapper<Lane>> right_adjacent_lanes;
  std::vector<RefWrapper<Lane>> successor_lanes;
  std::vector<RefWrapper<Lane>> antecessor_lanes;

  std::vector<RefWrapper<LaneBoundary>> left_lane_boundaries;
  std::vector<RefWrapper<LaneBoundary>> right_lane_boundaries;
  std::vector<RefWrapper<LaneBoundary>> free_lane_boundaries;

  RoadCondition road_condition;

  std::vector<ExternalReference> source_references;
};

/// @note In special cases only a shallow comparison of vector of Lane elements
/// could make sense. This function only compares the id, type and sub_type of
/// equivalent lanes. A deep comparison through all lane properties would be
/// problematic because the imap standard allows circular references.
/// E.g. the left_adjacent lane of lane id=4 is lane id=10.
/// And the left_adjacent lane of lane id=10 is lane id=4.
inline bool ShallowVectorLaneComparison(const std::vector<Lane>& lhs, const std::vector<Lane>& rhs) noexcept
{
  if (lhs.size() != rhs.size())
  {
    return false;
  }

  return std::equal(
      lhs.begin(), lhs.end(), rhs.begin(), [](const Lane& lhs_element, const Lane& rhs_element)
      { return lhs_element.id == rhs_element.id &&
               lhs_element.type == rhs_element.type &&
               lhs_element.sub_type == rhs_element.sub_type; });
}

inline bool ShallowVectorLaneComparison(const std::vector<RefWrapper<Lane>>& lhs, const std::vector<RefWrapper<Lane>>& rhs) noexcept
{
  std::vector<map_api::Lane> extracted_lhs(lhs.begin(), lhs.end());
  std::vector<map_api::Lane> extracted_rhs(rhs.begin(), rhs.end());

  return ShallowVectorLaneComparison(extracted_lhs, extracted_rhs);
}

inline bool ShallowVectorLaneComparison(const std::vector<RefWrapper<Lane>>& lhs, const std::vector<Lane>& rhs) noexcept
{
  std::vector<map_api::Lane> extracted_lhs(lhs.begin(), lhs.end());

  return (extracted_lhs == rhs);
}

inline bool ShallowVectorLaneComparison(const std::vector<Lane>& lhs, const std::vector<RefWrapper<Lane>>& rhs) noexcept
{
  std::vector<map_api::Lane> extracted_rhs(rhs.begin(), rhs.end());

  return (lhs == extracted_rhs);
}

inline bool operator==(const Lane& lhs, const Lane& rhs) noexcept
{
  if (std::tie(lhs.id,
               lhs.local_id,
               lhs.type,
               lhs.sub_type,
               lhs.centerline,
               lhs.left_lane_boundaries,
               lhs.right_lane_boundaries,
               lhs.free_lane_boundaries,
               lhs.road_condition,
               lhs.source_references) !=
      std::tie(rhs.id,
               rhs.local_id,
               rhs.type,
               rhs.sub_type,
               rhs.centerline,
               rhs.left_lane_boundaries,
               rhs.right_lane_boundaries,
               rhs.free_lane_boundaries,
               rhs.road_condition,
               rhs.source_references))
  {
    return false;
  }

  if (!ShallowVectorLaneComparison(lhs.left_adjacent_lanes, rhs.left_adjacent_lanes))
  {
    return false;
  }

  if (!ShallowVectorLaneComparison(lhs.right_adjacent_lanes, rhs.right_adjacent_lanes))
  {
    return false;
  }

  if (!ShallowVectorLaneComparison(lhs.successor_lanes, rhs.successor_lanes))
  {
    return false;
  }

  if (!ShallowVectorLaneComparison(lhs.antecessor_lanes, rhs.antecessor_lanes))
  {
    return false;
  }

  return true;
}

inline bool operator!=(const Lane& lhs, const Lane& rhs) noexcept
{
  return !(lhs == rhs);
}

inline bool operator==(const RefWrapper<Lane>& lhs, const Lane& rhs) noexcept
{
  return (lhs.get() == rhs);
}

inline bool operator!=(const RefWrapper<Lane>& lhs, const Lane& rhs) noexcept
{
  return !(lhs == rhs);
}

inline bool operator==(const RefWrapper<Lane>& lhs, const RefWrapper<Lane>& rhs) noexcept
{
  return (lhs.get() == rhs.get());
}

inline bool operator!=(const RefWrapper<Lane>& lhs, const RefWrapper<Lane>& rhs) noexcept
{
  return !(lhs == rhs);
}

inline bool operator==(const Lane& lhs, const RefWrapper<Lane>& rhs) noexcept
{
  return (lhs == rhs.get());
}

inline bool operator!=(const Lane& lhs, const RefWrapper<Lane>& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace map_api

#endif  // MAPSDK_MAPAPI_LANE_H
