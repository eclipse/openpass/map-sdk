# Contributing to Eclipse openPASS

This guide provides all necessary information to enable [contributors and committers](https://www.eclipse.org/projects/dev_process/#2_3_1_Contributors_and_Committers) to contribute to Eclipse openPASS. 

## Eclipse openPASS  
Eclipse openPASS provides a software platform that enables the simulation of traffic scenarios to predict the real-world effectiveness of advanced driver assistance systems or automated driving functions. 

## Developer resources

  * [Working Group Website](https://openpass.eclipse.org/)
  * [Developer Website](https://projects.eclipse.org/projects/automotive.openpass)
  * [Development process](https://gitlab.eclipse.org/groups/eclipse/openpass/-/wikis/home)
  * Mailing list: Join our [developer list](httphttps://projects.eclipse.org/projects/automotive.openpass/contact)
  * Bugs can be reported in [GitLab](https://gitlab.eclipse.org/groups/eclipse/openpass/-/issues/?sort=updated_desc&state=opened&first_page_size=100) by anybody who owns an Eclipse account. Please use type “Incident”
  * [Documentation](https://openpass.eclipse.org/resources/#documentation)

## Eclipse Contributor / Committer Agreement

Before your contribution can be accepted by the project team, contributors and committers must sign the correct agreement depending on their status. Please read on how to proceed on: https://www.eclipse.org/legal/committer_process/re-sign/.

For more information, please see the Eclipse Committer Handbook: https://www.eclipse.org/projects/handbook/#resources-commit.

## Contact

Contact the project developers via the project's "dev" list.

* openpass-dev@eclipse.org

## How to contribute

The openPASS source code can be found [here](https://gitlab.eclipse.org/eclipse/openpass/opSimulation).

The branch 'develop' contains the contributions that will be included in the next release. The 'main' branch contains the latest stable release.

A detailed development process can be found [here](https://gitlab.eclipse.org/groups/eclipse/openpass/-/wikis/home).
